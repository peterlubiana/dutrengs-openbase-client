/* 
*
*	UI.
* 	Code related to button presses, updates in the visual part of the UI for the user.
* * * * * * * */
dt.ui = {};

// Function that can ( and is being called)
// When a pressed the create User button after filling in the
// #CreateuserForm in the #CreateUserModal element.
dt.ui.createUserButtonPressed = function(){
	// Present create user UI

	// Get Data which was input from the new user form.
	var email = $('#CreateUserForm input[name=\'email\']').val();
	var password = $('#CreateUserForm input[name=\'password\']').val();

	if(dt.sys.isDebugging)
		alert(email + '  ' + password);

	// Attempt to create a new user
	dt.auth.createUser(email,password);

}

dt.ui.showUserProfile = function(){

}

dt.ui.forms = {};
dt.ui.forms.logInUserForm = $('#LogInUserForm');
dt.ui.forms.logInUserForm.on('submit',function(e){
	e.preventDefault();
	dt.ui.signInUserButtonPressed(e);
});


// This function is called when the user
// clicks a Login button, its task is to fetch data from the UI
// and attempt to login the user.
dt.ui.signInUserButtonPressed = function(e){
	e.preventDefault();
	var email = $('#LogInUserForm input[name=\'email\']').val();
	var password = $('#LogInUserForm input[name=\'password\']').val();

	if(dt.sys.isDebugging)
		alert(email + ' ' + password + '  tried to login.');

	// Log in the user with Firebase
	dt.auth.signInUser(email,password);
}

// This function is called whenever an appointment form has been
// filled, it takes data from the HTML-appointment-form and 
// attempts to create an appointment.
dt.ui.createAppointmentButtonPressed = function(){

	if(dt.sys.isDebugging){
		alert('Create Appointment button was pressed');
	}

	// Fetch data from the form.
	var ap = {};
	ap.title = $('#CreateAppointmentForm input[name=\'title\']').val();
	ap.description = $('#CreateAppointmentForm input[name=\'description\']').val();
	ap.time = $('#CreateAppointmentForm input[name=\'time\']').val();
	ap.minUsers = $('#CreateAppointmentForm input[name=\'minUsers\']').val();
	ap.maxUsers = $('#CreateAppointmentForm input[name=\'maxUsers\']').val();
	var lat = $('#CreateAppointmentForm input[name=\'lat\']').val();
	var lng = $('#CreateAppointmentForm input[name=\'lng\']').val();
	ap.latlng = {'lat':Number(lat),'lng':Number(lng)};
	ap.categories = $('#CreateAppointmentForm input[name=\'categories\']').val();
	ap.place = $('#CreateAppointmentForm input[name=\'place\']').val();

	// A simple local frontend Appointment validation function.
	function validateForm(){

		var valid = true;
		if(!ap.title || ap.title.length > 32){
			$('#CreateAppointmentForm input[name=\'title\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'title\']').css('border-color','green');
		}

		if(!ap.description || ap.description.length > 256){
			$('#CreateAppointmentForm input[name=\'description\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'description\']').css('border-color','green');
		}
			
		if(!ap.time){
			$('#CreateAppointmentForm input[name=\'time\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'time\']').css('border-color','green');
		}
		if(!ap.maxUsers || ap.maxUsers > 16){
			$('#CreateAppointmentForm input[name=\'maxUsers\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'maxUsers\']').css('border-color','green');
		}

		if(!ap.latlng.lat || !ap.latlng.lng){
			$('#CreateAppointmentForm .noPostitionSetError').show();
			valid = false;
		}else{
			$('#CreateAppointmentForm .noPostitionSetError').hide();
		}
		if(!ap.minUsers || ap.maxUsers < ap.minUsers || ap.minUsers < 2){
			$('#CreateAppointmentForm input[name=\'minUsers\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'minUsers\']').css('border-color','green');
		}
		
		if(!ap.categories){
			$('#CreateAppointmentForm input[name=\'categories\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'categories\']').css('border-color','green');
		}
		if(!ap.place || ap.place.length > 64){
			$('#CreateAppointmentForm input[name=\'place\']').css('border-color','red');
			valid = false;
		}else{
			$('#CreateAppointmentForm input[name=\'place\']').css('border-color','green');
		}
		return valid;
	}

	// Some simple frontend validation
	// More relentless validation is performed serverside.
	var validated = validateForm();
	if(validated){

		// Deugging message.
		if(dt.sys.isDebugging)
		 	alert('User attempted to create an appointment');

		 // Attempt to create an appointment 
		 // and store it in the database.
		 dt.db.addAppointment(ap);
		 
	}else{

		// Debugging message.
		if(dt.sys.isDebugging)
			alert('User failed to create an appointment');

		alert('Vennligst fyll ut alle feltene korrekt i skjemaet.');
	}
}

// This function changes the 'main-screen' / tabs of the webapp.
// Its where most of the 'action' is supposed to be happening.
// With 'Activity' i mean what the user is currently doing.
// Like looking at his/hers profile / managing activities / browsing activities etc.
dt.ui.setMainActivity = function(activityname){

	if(dt.sys.isDebugging){
		alert('activity switched to: ' + activityname);
	}
	// Hide all activities divs.
	$('activity').hide();

		switch(activityname){
			case 'browseactivities':
				$('#ActivityBrowseActivities').show();

				// When the map is dragged, and the dragging stops
				// This functions fetches ALL the appointments in firebase.database().ref("appointments")
				// And sorts it on the client-side, not perfect, but works for now.
				dt.map.mainMap.addListener('idle',function(){
					dt.db.getAppointmentsWithinMap(dt.map.mainMap,20,function(aptList){
						// Empty the SearchResult-element.
						$('#SearchResultsActivityList').html('');
						// Remove all Appointment-markers on the map.
						dt.map.clearMarkers(dt.map.mainMap);
						// Google maps, don't refresh/load properly unless resized
						// explicitly through action, hence this call.
						google.maps.event.trigger(dt.map.mainMap, 'resize');

						// Loop through the AppointmentList which was retrieved
						// from dt.db.getAppointmentsWithinMap
						if(aptList){
							for(var a in aptList){
								// a is the ID of the marker.
								var appointmentData = aptList[a];
								// Create Appointment-HTML and put it in the searchresults area.
								$('#SearchResultsActivityList').append(dt.ui.createAppointmentItemHTML(a,appointmentData));

								// Add markers to the main map.
								dt.map.addAppointmentMarker(dt.map.mainMap,a,appointmentData);
							}	
						}	
					});
				});

				// Kick off some events to make the map respond.
				// If we dont it will remain gray and there will be no event in the beginning.
				google.maps.event.trigger(dt.map.mainMap, 'resize');
				google.maps.event.trigger(dt.map.mainMap, 'dragend');
				
			break;


			case 'showyouractivities':
				$('#ActivityYourActivities').show();
				
				// Get all the Appointments current user has created.
				dt.db.getAppointmentsCreatedByCurrentUser(function(aptList){

					// Empty the current List.
					$('#UserCreatedAppointmentList').html('');

					if(aptList){
						for(var a in aptList){
							var appointmentData = aptList[a];
							$('#UserCreatedAppointmentList').append(dt.ui.createAppointmentItemHTML(a,appointmentData));
						}
					}						
				});

				// Get all the appointments the current user is participating in.
				dt.db.getAppointmentsWhereUserParticipates(function(aptList){

					// Empty the current List.
					$('#UserParticipatesAppointmentList').html('');

					if(aptList){
						for(var a in aptList){
							var appointmentData = aptList[a];
							$('#UserParticipatesAppointmentList').append(dt.ui.createAppointmentItemHTML(a,appointmentData));
						}
					}
				});
				
				

			break;

			case 'showprofile':
				$('#ActivityShowProfile').show();
				// This function fills the #ActivityShowProfile div
				// With data about the current Firebase-User.
				dt.ui.setProfileUI(dt.auth.currentUser);
			break;

		}
}

// Function that is called when a
// Button meant to show a full appointment is pressed.
dt.ui.showAppointmentButtonPressed = function(e){


	// The event handler can get sub-elements of the appointment-element
	// So here we make sure we get the correct element.
	var appointmentElement = e.target.parentNode;

	if(dt.sys.isDebugging){
		alert('An appointment was pressed.');
		console.log('Element which was pressed: ');
		console.dir(appointmentElement);
		alert('Appointment ID.' + appointmentElement.dataset.appointmentid);
	}

	// Get appointmentID from the element which was pressed.
	var appointmentID = appointmentElement.dataset.appointmentid;


	// Push HTML to the Appointment displayer-element.
	dt.ui.displaySpecificAppointmentInElement(appointmentID,'SpecificAppointmentDisplayArea');

}

// Setup appointment buttons for clicks.
$(document).on('click','.showFullAppointmentButton',dt.ui.showAppointmentButtonPressed);



// Creates a small appointmentHTML element.
// An miniature version of the Appointment Item.
// Returns HTML.
dt.ui.createAppointmentItemHTML = function(appointmentID, appointmentData){
	var data = appointmentData;
	if(dt.sys.isDebugging){
		console.log('appointment data when creating HTML: ');
		console.dir(data);
	}
	var html = '<appointment data-appointmentID="'+appointmentID+'">';
	html += '<div name="title" class="showFullAppointmentButton" data-toggle="modal" data-target="#DisplayAppointmentModal">'+data.title+'</div>';
	html += '<div name="description"> '+data.description+'</div>';
	html += '<div name="time"> <b>Når?</b> '+data.time+'</div>';
	html += '<div name="place"> <b>Hvor?</b> '+data.place+'</div>';

	// Add Map.
	html += dt.ui.createGoogleMapsHTMLForLatLng(data.latlng.lat,data.latlng.lng,'200px','160px');

	html += dt.ui.createParticipantsListHTML(data);
	// Only render take part button if the user is not also administrator
	if(data.administrator != firebase.auth().currentUser.uid){
		// Also the user must not be a part of the appointment already.
		// If the users ID is in the participating users array.
		if(!data.participatingUsers[firebase.auth().currentUser.uid])
			html += '<div class="btn btn-purple joinAppointmentButton"> Bli med! </div>';
		else
			html += '<div class="btn btn-warning leaveAppointmentButton">Forlat Aktivitet </div>';
	}else{
		// Else add a delete button to the appointment
		html += '<div class="btn btn-danger deleteAppointmentButton"> Slett Aktivitet </div>';
		html += '<div class="btn btn-secondary openEditAppointmentModalButton" data-toggle="modal" data-target="#EditAppointmentModal"> Endre </div>';
	}

	

	html += '<div class="btn btn-success showFullAppointmentButton" data-toggle="modal" data-target="#DisplayAppointmentModal"> Mer. </div>';
	html += '</aappointment>';

	
	return html;
}

// Creates a Full standalone Appointment HTML-element.
// Returns HTML which can be put anywhere.
dt.ui.createAppointmentBigHTML = function(appointmentID, appointmentData){
	var data = appointmentData;
	if(dt.sys.isDebugging){
		console.log('appointment data when creating HTML: ');
		console.dir(data);
	}
	var html = '<appointmentbig data-appointmentID="'+appointmentID+'">';
	html += '<h3 name="title"> '+data.title+'</h3>';
	html += '<div name="description"> '+data.description+'</div>';
	html += '<div name="time"> <b>Når?</b> '+data.time+'</div>';
	html += '<div name="place"> <b>Hvor?</b> '+data.place+'</div>';

	// Add Map.
	html += dt.ui.createGoogleMapsHTMLForLatLng(data.latlng.lat,data.latlng.lng,'100%','100%');

	//html += '<div name="participants"> <b>Deltagere</b> '+data.participatingUsers+'</div>';

	html += dt.ui.createParticipantsListHTML(data);

	html += '<div name="participantsCount"> <b> '+data.minUsers+'-'+data.maxUsers+' deltagere</b> </div>';
	html += '<div name="administrator"> <b>Initiativtaker</b> '+data.administrator+'</div>';
	//html += '<div name="messageboard"> <b>Meldinger</b> '+data.messages+'</div>';

	// Only render take part button if the user is not also administrator
	if(data.administrator != firebase.auth().currentUser.uid){
		// Also the user must not be a part of the appointment already.
		// If the users ID is in the participating users array.
		if(!data.participatingUsers[firebase.auth().currentUser.uid])
			html += '<div class="btn btn-purple joinAppointmentButton"> Bli med! </div>';
		else{
			html += '<div class="btn btn-warning leaveAppointmentButton">Forlat Aktivitet </div>';
		}
	}else{
		// Else add a delete button to the appointment
		html += '<div class="btn btn-danger deleteAppointmentButton"> Slett Aktivitet </div>';
		html += '<div class="btn btn-secondary openEditAppointmentModalButton" data-toggle="modal" data-target="#EditAppointmentModal"> Endre </div>';
	}


	// Only render the messageBoard for the logged-in admin and for users that participate.
	if(data.administrator == firebase.auth().currentUser.uid || data.participatingUsers[firebase.auth().currentUser.uid]){
		// Add the message-board
		html += dt.ui.createMessageBoard(data);	
	}
	


	html += '<appointmentbig/>';
	return html;
}

// Creates a HTML-list over users participating in an event
// Takes A full appointment as data, and returns a 
// List of images ( not related to users ) of how many
// Users are participating, and how many open spots there are.
dt.ui.createParticipantsListHTML = function(data){
	var html = '';
	// Print out filled spot images.
			var userCount = 0;
	html += '<div class="participantList">';
	for(var user in data.participatingUsers){
		html += '<img class="imageSpotTaken" width="24px" height="24px" src="'+ dt.sys.defaultProfileImageUrl+'"/>';
		userCount++;
	}

	// Print out empty spot images.
	if(userCount < data.maxUsers){
		for(var i = 0 ; i < data.maxUsers-userCount;i++){
			html += '<img class="imageSpotOpen" width="24px" height="24px" src="'+ dt.sys.emptySpotImageUrl +'"/>';
		}
	}
	html+= '</div>'; // To finish of the .participantList
	return html;
}

// Creates a html Messageboard based on appointmentData 
// Which it gets as its only argument and returns HTML.
dt.ui.createMessageBoard = function(appointmentData){
	var data = appointmentData;
	var html = '<div class=\'messageBoard\'>';
	html += '<h4> Meldinger: </h4>';
	// If there even are any messages
	if(!data.messages){
		html += '<div> Det er ingen meldinger skrevet her enda.</div>';
	}else{
		// Render a messageList if there are any messages.
		html += '<div class=\'messageList\'>';
		for(var msgKey in data.messages){
			html += '<appointmentmessage>';
			html += '<div class="messageSender"> ' + data.messages[msgKey].sender + '</div>';
			html += '<div class="messageTime">  ' + data.messages[msgKey].time + '</div>';
			html += '<div class="messageContent">' + data.messages[msgKey].content + '</div>';
			html += '</appointmentmessage>';
		}
		html += '</div> <!-- messageList ends -->';
	}

	// An input-form where messages can be written.
	html += '<form class="messageInputForm">';
	html += '<textarea name="content" placeholder="Din melding." />';
	html += '<input class="btn btn-purple sendMessageToAppointmentButton" type="submit" value="Send" />';
	html += '</form> <!-- .messageInputForm ends -->';

	html += '</div> <!-- .messageBoard ends -->';
	return html;

}

dt.ui.getCurrentTimeAndDate = function(){
	var d = new Date();
	
	// add a 0 if anything returns a single digit.
	var minute = String(d.getMinutes()).length == 1 ? '0'+d.getMinutes() : d.getMinutes();
	var hour = String(d.getHours()).length == 1 ? '0'+d.getHours() : d.getHours();
	var date = String(d.getDate()).length == 1 ? '0'+d.getDate() : d.getDate();
	var month = String(d.getMonth()).length == 1 ? '0'+(d.getMonth()+1) : (d.getMonth()+1);

	return hour + ':' + minute + ' ' + date + '.' + month + '.' + d.getFullYear();

}

// Takes message info from the UI and sends it to firebase for storage.
dt.ui.sendMessageButtonPressed = function(e){

	e.preventDefault();

	// Get appointmentID
	// Dig upwards in the parent-elements for the appointmentID
	while(!e.target.dataset['appointmentid']){
		e.target = e.target.parentNode;
	}
	var appointmentID = e.target.dataset.appointmentid;

	// Get the current user.
	var sender = firebase.auth().currentUser.uid;

	// Get the current Time and Date
	var time = dt.ui.getCurrentTimeAndDate();

	// Get message from the UI.
	var content = $(e.target).find('textarea[name=\'content\']').val();

	var messageObj = new dt.class.Message({'sender':sender,'time':time,'content':content});

	if(dt.sys.isDebugging){
		alert('message object : ' + messageObj);
		console.log('messageObject to be sent to firebase.');
		console.log(messageObj);
	}

	// Actually send the message to be stored in firebase.
	dt.db.addMessageToAppointment(messageObj,appointmentID,function(ok){
		if(ok){
			alert('Meldingen din ble lagret!');
		}else{
			alert('Meldingen din ble ikke lagret! Prøv igjen senere.');
		}
	});
	
	
}

$(document).on('click','.sendMessageToAppointmentButton',dt.ui.sendMessageButtonPressed);




// Creates a static googleMapsHTML img
// WARNING:
// This google API might be gone in not too long,
// So it would be wise to have a backup-plan.
dt.ui.createGoogleMapsHTMLForLatLng = function(lat,lng,w,h){
	return '<img width="'+w+'" height="'+h+'" src="http://maps.googleapis.com/maps/api/staticmap?center='+lat+','+lng+'&zoom=14&size=400x300&sensor=false&markers=color:blue%7Clabel:S%7C'+lat+','+lng+'">';
}

// This function fills the show Profile area with
// the correct data for the current user
dt.ui.setProfileUI = function(user){
	var $namePanel = $('#UserProfilePanel .userName');
	var $mailPanel = $('#UserProfilePanel .email');
	var $photoPanel= $('#UserProfilePanel .profilePhoto');

	$namePanel.html(user.displayName);
	$mailPanel.html(user.email);
	$photoPanel.attr('src',dt.sys.defaultProfileImageUrl);
}

// This function displays somehow a 
// specific Appointment to the user.
dt.ui.displaySpecificAppointmentInElement = function(appointmentid,parent_element){
	// Fetch appointment data, and listen to changes on the appointment.
	dt.db.getAppointmentByIDWatcher(appointmentid,function(data){
		var aptHTML = dt.ui.createAppointmentBigHTML(appointmentid,data);
		$('#'+parent_element).html(aptHTML);
	});
}


dt.ui.openCreateAppointmentModalPressed = function(e){
	// Since the dt.ui.getLatLngFromMap creates
	// A map in both the #CreateAppointmentForm .displayMap
	// #EditAppointmentModal .displayMap to simplify that function
	// This call needs to be done in case the user first edits
	// an appointment, and then suddenly Creates an appointment after.
	// Should be fixed.
	$('#CreateAppointmentForm .displayMap').html('');

}

// Make sure clicks to .openCreateAppointmentModalButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.openCreateAppointmentModalButton',dt.ui.openCreateAppointmentModalPressed);


// This function grabs an AppointmentID from an <appoinment>-element
// or <appointmentbig>-element and then goes to Firebase to get the
// Data for the appointment using its ID, before it puts the data
// Into the various <inputs> of the #EditAppointmentModal preparing the
// Modal for updating an appointment.
dt.ui.openEditAppointmentModalButtonPressed = function(e){
	if(dt.sys.isDebugging){
		alert('dt.ui.openEditAppointmentModalButtonPressed');
	}

	// Get correct appointment
	var appointmentElement = e.target.parentNode;
	var appointmentID = appointmentElement.dataset.appointmentid;

	dt.db.getAppointmentByID(appointmentID,function(data){
		// Write the appointmentdata into the 
		// Update appointment form.

		if(dt.sys.isDebugging){
			alert('appointment fetched from inside openEditAppointmentModalButtonPressed');
			alert(data);
		}

		$('#EditAppointmentForm').attr({'data-appointmentID':appointmentID});

		$('#EditAppointmentModal input[name=\'title\']').val(data.title);
		$('#EditAppointmentModal input[name=\'description\']').val(data.description);
		$('#EditAppointmentModal input[name=\'time\']').val(data.time);
		$('#EditAppointmentModal input[name=\'place\']').val(data.place);
		$('#EditAppointmentModal input[name=\'lat\']').val(data.latlng.lat);
		$('#EditAppointmentModal input[name=\'lng\']').val(data.latlng.lng);
		$('#EditAppointmentModal .displayMap').html(dt.ui.createGoogleMapsHTMLForLatLng(data.latlng.lat,data.latlng.lng,'100%','100%'));
		$('#EditAppointmentModal input[name=\'categories\']').val(data.categories);

	});

}

// Make sure clicks to .openEditAppointmentModalButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.openEditAppointmentModalButton',dt.ui.openEditAppointmentModalButtonPressed);



// Function that is called when the UpdateAppointmentButton is pressed
// This function fetches data form the UI and pushes it up to the 
// Database.
dt.ui.updateAppointmentButtonPressed = function(e){

	if(dt.sys.isDebugging){
		alert('update Appointment Button was pressed!');
		alert(e);
	}

	// Get correct appointment
	var appointmentElement = e.target.parentNode;
	var appointmentID = appointmentElement.dataset.appointmentid;

	if(dt.sys.isDebugging)
		alert('appointmentID to be updated : '+appointmentID);

	// Prepare data for changing.
	var newData = {} 
	newData.title = $('#EditAppointmentModal input[name=\'title\']').val();
	newData.description = $('#EditAppointmentModal input[name=\'description\']').val();
	newData.time = $('#EditAppointmentModal input[name=\'time\']').val();
	var lat = $('#EditAppointmentModal input[name=\'lat\']').val();
	var lng = $('#EditAppointmentModal input[name=\'lng\']').val();
	newData.latlng = {'lat':Number(lat),'lng':Number(lng)};
	newData.place = $('#EditAppointmentModal input[name=\'place\']').val();
	newData.categories = $('#EditAppointmentModal input[name=\'categories\']').val();

	// Write to the database.
	dt.db.updateAppointmentById(appointmentID,newData,function(ok){
		if(ok){
			$('#EditAppointmentModal').modal('hide');
			dt.ui.setMainActivity('browseactivities');
		}else{
			alert('Endringen ble ikke utført.');
		}
	});

}
// Make sure clicks to .updateAppointmentButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.updateAppointmentButton',dt.ui.updateAppointmentButtonPressed);




// This is a messy function that needs cleaning
// It grabs an appointmentID from its parent element
// And checks if the appointment has an empty spot
// and then it Adds a user to an appointment.
// Needs some testing as it stands right now.
dt.ui.takePartInAppointmentButtonPressed = function(e){
	if(dt.sys.isDebugging){
		alert('Take part in Appointment Button was pressed!');
		alert(e);
	}
	// Get correct appointment
	var appointmentElement = e.target.parentNode;
	var appointmentID = appointmentElement.dataset.appointmentid;

	// Check if its free
	dt.db.getEmptySpotCountForAppointmentByID(appointmentID,function(num){
		if(!num || num == 0){
			if(dt.sys.isDebugging)
				alert('The event doesnt exist or is full!');
		}else{
			// attempt to add user to the appointment
			dt.db.addUserToAppointmentById(firebase.auth().currentUser.uid,appointmentID,function(err){
				if(err){
					alert('Du er nå en del av denne aktiviteten!');
				}else{
					if(dt.sys.isDebugging){
						alert('User joined an Appointment.');
					}
					alert('Aktiviteten er enten full eller noe annet gikk galt. Prøv igjen senere.');
				}
			});
		}

	});

}
// Make sure clicks to .joinAppointmentButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.joinAppointmentButton',dt.ui.takePartInAppointmentButtonPressed);


// Deletes an appointment from firebase.database().ref("appointment/appointmentID)
dt.ui.deleteAppointmentButtonPressed = function(e){
	if(dt.sys.isDebugging){
		alert('deleteAppointmentButton was pressed!');
		alert(e);
	}
	// Get correct appointment
	var appointmentElement = e.target.parentNode;
	var appointmentID = appointmentElement.dataset.appointmentid;
	dt.db.deleteAppointmentById(appointmentID,function(data){
		if(data){
			// Was not deleted.
			alert('Aktiviteten ble ikke slettet fra systemet. Prøv igjen.');
		}else{
			// Was deleted
			alert('Aktiviteten ble slettet fra systemet.');

			// Hide display appointment modal if it is visible atm.
			$('#DisplayAppointmentModal').modal('hide');
		}
	});
}

// Make sure clicks to .deleteAppointmentButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.deleteAppointmentButton',dt.ui.deleteAppointmentButtonPressed);




// This function is called whenever a 
// button with class .leaveAppointmentButton is pressed.
dt.ui.leaveAppointmentButtonPressed = function(e){

	if(dt.sys.isDebugging){
		alert('dt.ui.leaveAppointmentButton was pressed!');
		alert('element that was pressed ');
	}
	// Find the appointment which was pressed
	var appointmentElement = e.target.parentNode;
	var appointmentID = appointmentElement.dataset.appointmentid;

	// Check with the database if the user is in the appointment
	dt.db.getParticipatingUsersInAppointment(appointmentID,function(userList){
		if(!userList){
			if(dt.sys.isDebugging){
				alert('The user is not in the List.')	
			}
		}

		for(var u in userList){
			if(userList[u] == firebase.auth().currentUser.uid){
				// Attempt to remove the user from the appointment
				dt.db.removeUserFromAppointment(firebase.auth().currentUser.uid,appointmentID,function(data){
						// The user should have been removed. 
				});
			}
		}
	});
}

// Make sure clicks to .leaveAppointmentButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.leaveAppointmentButton',dt.ui.leaveAppointmentButtonPressed);


// This function opens the dt.map.getLatLngMap
// triggers its resize event to make it refresh
// Attatches and waits for a click on the map
// Getting a latitide and longitude which
// is passed as an argument to the callback (cb)
// For futher processing.
dt.ui.getLatLngFromMap = function(cb){
	$('#getLatLngMap').show();
	// WHen clicked return and call the callback with the dataz.
	google.maps.event.trigger(dt.map.getLatLngMap, 'resize');

	 dt.map.getLatLngMap.addListener('click',function(e){
        	// e.latLng contains where user clicked.
        	if(dt.sys.isDebugging){
        		alert('getLatLngMap was clicked;) ');
        		console.log('getLatLngMapPressed event-object:');
        		console.log(e);
        	}
        	$('#getLatLngMap').hide();
        	cb(e.latLng);

        });

}

// Calls the function that opens a map that users
// can use to select a latitude and longitude.
dt.ui.getLatLngButtonPressed = function(e){
	dt.ui.getLatLngFromMap(function(data){
		if(dt.sys.isDebugging){
			alert('Latlng data ' + data);
			console.log('latLng data: ');
			console.log(data);
		}
		
		// Stores the latitude/longitide in a 
		// variable, which can be used later.
		dt.map.selectedLatLng = data;

		// Store the new lat-lng data in the EditAppointment-modal.
		$('#EditAppointmentModal input[name=\'lat\']').val(data.lat());
		$('#EditAppointmentModal input[name=\'lng\']').val(data.lng());
		$('#EditAppointmentModal .displayMap').html(dt.ui.createGoogleMapsHTMLForLatLng(data.lat(),data.lng(),'100%','100%'));

		// Store the new lat-lng data in the CreateAppointment-modal.
		$('#CreateAppointmentForm input[name=\'lat\']').val(data.lat());
		$('#CreateAppointmentForm input[name=\'lng\']').val(data.lng());
		$('#CreateAppointmentForm .displayMap').html(dt.ui.createGoogleMapsHTMLForLatLng(data.lat(),data.lng(),'100%','100%'));
	})
}

// Make sure clicks to .getLatLngMapButton's are heard
// no matter if they exist at the time of binding this handler.
$(document).on('click','.getLatLngMapButton',dt.ui.getLatLngButtonPressed);


// This function is called to update the Entire UI of the webapp
// Whenever a user logs in or out. The user argument is a 
// firebase.auth().user with lots of properties
dt.ui.toggleLoggedInUI =  function(user){
	// The user is Logged In.

	// Delay the changes slightly.
	setTimeout(changeUserUI,500);

	// Local function which was given a name
	// so the setTimeout would look more sexy.
	function changeUserUI(){
		if(user){

			// Hide CreateUserModal
			$('#CreateUserModal').modal('hide');

			// Hide login-form.
			$('#LogInUserForm').hide();

			// Hide Pitch
			$('#pitch').hide();

			// Show logged in main menu.
			$('#MainMenu').show();

			// Hide argumentation
			$('.argumentation').hide();

			// Hide CurrentActivityPanel
			$('#CurrentActivityPanel').show();

			dt.ui.setMainActivity('browseactivities');

			// Show profile panel(s)
			$('#UserNavPanel').show();
			$('#UserNavPanel .displayName').html(user.displayName);
			$('#UserNavPanel .email').html(user.email);
			$('#UserNavPanel .profilePhoto').attr('src','images/defaultprofileimage.png');


		}else{

			// Hide Pitch
			$('#pitch').show();

			// Hide UserNavPanel
			$('#UserNavPanel').hide();

			// Hide logged in main menu.
			$('#MainMenu').hide();

			// Show LoginForm
			$('#LogInUserForm').show();

			// Show argumentation
			$('.argumentation').show();

			// Hide CurrentActivityPanel
			$('#CurrentActivityPanel').hide();


		}
	}
}

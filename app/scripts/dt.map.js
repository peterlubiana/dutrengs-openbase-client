/*
* 	Map Management
* * * * * * * * * * * */
dt.map = {};
dt.map.activated = true;
dt.map.selectedLatLng = {lat: 0, lng: 0};

// Creates and confiugures the maps used in this webapp.
dt.map.init = function(){
	if(dt.map.activated){

         // Default pos
        var oslo = {lat: 59.892380129154624, lng: 10.735788682373032};

        var startPos = oslo;


        // Main Map
        dt.map.mainMap = new google.maps.Map(document.getElementById('mainMap'), {
          zoom: 9,
          center: startPos,
          disableDefaultUI: true
        });

        dt.map.mainMap.markers = [];

        // getLatLngmap ( Used to get LatNLng from users.)
        dt.map.getLatLngMap = new google.maps.Map(document.getElementById('getLatLngMap'), {
          zoom: 7,
          center: startPos
        });

        dt.map.getLatLngMap.markers = [];





        //Søke-boks som bruker google places.
        var searchBox = new google.maps.places.SearchBox(document.getElementById('sted-input'));

        google.maps.event.addListener(searchBox , 'places_changed' , function(){
            var places = searchBox.getPlaces();
            var bounds =  new google.maps.LatLngBounds();
            var i,place;
            for( i = 0; place = places[i]; i++)
            {
            bounds.extend(place.geometry.location);
            }
            dt.map.mainMap.fitBounds(bounds);
            dt.map.mainMap.setZoom(12);
           // alert("place changed to" + place.geometry.location);
        });

        dt.map.mainMap.addListener('bounds_changed', function() {
          searchBox.setBounds(dt.map.mainMap.getBounds());
        });


	}
}

// Adds an appointment marker to a google.maps map.
dt.map.addAppointmentMarker = function(map,appointmentID,appointmentData){
	var icon_url = 'images/markericon.png';

	// Converting the lat and lng's to numbers
	// in case they were stored as strings.
	var _lat = Number(appointmentData.latlng.lat);
	var _lng = Number(appointmentData.latlng.lng);

	var marker = new google.maps.Marker({
      position: {'lat':_lat,'lng':_lng},
      map: map,
      animation: google.maps.Animation.DROP,
      label: {
      		'text':appointmentData.title,
      		'color':'white',
      },
      title: appointmentData.title,
      icon: icon_url
    });

    map.markers.push(marker);

    // Attatches a onclick-handler to the appointment
    // To enable it to be interacted with for whatever purpose we please.
    marker.addListener('click',function(e){
    	if(dt.sys.isDebugging){
    		alert('marker with ID ' + appointmentID +' was clicked.');	
    	}

    	dt.ui.displaySpecificAppointmentInElement(appointmentID,'SpecificAppointmentDisplayArea');
    	$('#DisplayAppointmentModal').modal('show');
    });
}

// Deletes all the markers added on a map.
dt.map.clearMarkers = function(map){
	if(map.markers){
		for(var i = 0 ; i < map.markers.length ; i++){
			map.markers[i].setMap(null);
		}
		map.markers = [];
	}
}
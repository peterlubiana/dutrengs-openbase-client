
// Create the global dt (dutrengs)-object.
// This is the equivalent to the global 'App' variable which many other systems use
// This exposes it to be used wherever we please but also to just take up 1 top-level
// variable.
var dt = function(){
	var dt = {};

	// This function is called when the document is loaded.
	dt.init = function(){
		dt.auth.init();
		
		if(openbase){
			console.log('Openbase was loaded.')
		}
	}



	/* System Options 
	*  Special contants / functions that dont fit into the other categories
	*  at the moment.
	* * * * * */
	dt.sys = {};
	dt.sys.isDebugging = true; // If you set this to true you will get tons of debugging alerts and console.logs.
	dt.sys.defaultProfileImageUrl = 'images/defaultprofileimage.png';
	dt.sys.emptySpotImageUrl = 'images/emptyspotimage.png';


	/*
	*
	* Authentification.
	* Functions that deal with logging in and out of the system
	* as well as storing information about the current logged-in user.
	* * * * * * * * * * * * * * * * * */
	dt.auth = {};


	dt.auth.currentUser = undefined;
	dt.auth.init = function(){

		openbase.init({
			domain:"http://128.199.33.216:3000",
			pollTime:2000
		});


		openbase.auth.onAuthChanged(function(user) {
			 dt.auth.currentUser = user; // user is undefined if no user signed in

			 dt.ui.toggleLoggedInUI(user);

			 if(dt.sys.isDebugging){
			 	if(user){
	  				alert('A user logged in'+user);
	  				console.log('User that logged in:');
		  			console.dir(user);
	  			}else{
	  				alert('A user logged out.');
	  			}
	  		}

	  		if(user && !user.displayName){
	  			dt.auth.updateUsersDisplayName(user,'Meg');
	  		}



		});
	}

	/*
	This function updates a firebase user's displayName
	( Not any user store in the firebase.database()   )
	*/
	dt.auth.updateUsersDisplayName = function(user,name){
		openbase.auth.updateUser({"displayName":name,"photoURL":dt.sys.defaultProfileImageUrl})
	}
	/*
	*	This function creates a User with firebases built-in create-user system.
	*/
	dt.auth.createUser = function(mail,password){
		firebase.auth().createUserWithEmailAndPassword(mail, password).then(function(user){

			dt.auth.updateUsersDisplayName(user,'Meg');


		}).catch(function(error) {
		  // Handle Errors here.
 		 var errorCode = error.code;
 		 var errorMessage = error.message;

 		 if(dt.sys.isDebugging)
	  		alert(error.code + '\n' + error.message);



  		// ...
		});
	}

	/*
		Sign out a user with Firebase's built-in user-system
	*/
	dt.auth.signOutUser = function(){
		openbase.auth.signOut();
	}

	/*
		Sign in a user with Firebase's user-system.
	*/
	dt.auth.signInUser = function(email,password){
		openbase.auth.signIn(email, password,function(){
			alert("signin thing finished");
		});

	}





	/*
	*	Classes
	*   - Datastructures used in the system.
	* * * * * * * * */

	dt.class = {};

	// Class for creating new Appointments
	dt.class.Appointment = function(data){
		this.title = data.title;
		this.description = data.description;
		this.time = data.time;
		var adminID = firebase.auth().currentUser.uid;
		this.participatingUsers = {adminID:adminID};
		this.minUsers = Number(data.minUsers);
		this.maxUsers = Number(data.maxUsers);
		this.categories = data.categories;
		this.administrator = firebase.auth().currentUser.uid;
		this.latlng = {'lat':Math.random()*100,'lng':Math.random()*100};
		this.place = data.place;
		this.messages = {};
	}

	// Class for creating new Users
	dt.class.User = function(data){
		this.userID = data.userID;
		this.displayName = data.displayName;
		this.appointments = [];
		this.previousAppointments = [];
	}

	// Class for creaeting new Messages
	dt.class.Message = function(data){
		this.sender = data.sender;
		this.time   = data.time;
		this.content = data.content;
	}

	// returns the local variable dt-object to the anonymous
	// function that called it.
	return dt;
}();



// 'Start' the DuTrengs-webapp when the document has loaded.
$(document).ready(function(){
	dt.init();
});

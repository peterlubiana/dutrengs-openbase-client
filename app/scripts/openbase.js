
var app = {};


app.sys = {};
app.sys.domain = "/";
app.sys.isDebugging = true;


app.init = function(config){
	if(!config.pollTime){
		config.pollTime = 1000;
	}
	app.sys.domain = config.domain ? config.domain : "/";
	app.auth.stopPolling();
	app.auth.pollAuthenticated(config.pollTime)
}


app.auth = {};
app.auth.currentUser = undefined;
app.auth.pollInterval = undefined;
app.auth.prevState = false;
app.auth.loggedIn  = false;

app.auth.signOut = function(cb){
	$.get(app.sys.domain+"/logoutUser",undefined,function(){
		if(cb)
			cb();
	});
}

app.auth.signIn = function(mail,pw,cb){
	$.post(app.sys.domain+"/loginUser",{"email":mail,"password":pw},function(){
		if(cb)
			cb();
	});
}

app.auth.pollAuthenticated = function(timeBetweenPolls){
	app.auth.pollInterval = setInterval(function(){
		$.get(app.sys.domain+"/isLoggedIn",undefined).done(function(data){

		}).always(function(data){

			app.auth.prevState = app.auth.loggedIn;
			app.auth.loggedIn = data.status ? false : true;

			if(app.sys.isDebugging){
				console.log(data);
				console.log(app.auth.prevState);
				console.log(app.auth.loggedIn);
			}

			if(app.auth.prevState != app.auth.loggedIn){
				var local_data = data.status ? null : data;
				app.auth.onAuthChanged(local_data);
				app.auth.currentUser = local_data;
			}
		});
	},timeBetweenPolls);
}

app.auth.stopPolling = function(){
	clearInterval(app.auth.pollInterval);
}


// User API
app.auth.onAuthChanged = function(userData){
}

app.auth.createUser = function(email,password,cb){
	// send a request to 
	$.post(app.sys.domain+"/createUser",{"email":email,"password":password}).done(function(data){
		cb(data);
	});

}

// Write updates to the database.
app.auth.updateUser = function(data,callback){	
	$.post(app.sys.domain+"/updateUser",data).done(function(data){
		callback(data);
	});
};





// Write to database-API.
app.db = {};
app.db.set = function(route,data,callback){

	if(app.sys.isDebugging){
		console.log("Database writeRequest sent for route " + route + "with data: ");
		console.dir(data);
	}

	$.post(app.sys.domain+"/writeRequest",{"___writePath":route,'data':data}).done(function(data){
		callback(data);
	});
};

app.db.getDataByRoute = function(route,callback){
	$.get(app.sys.domain+route,{}).done(function(data){
		callback(data);
	});
}


var openbase = app;



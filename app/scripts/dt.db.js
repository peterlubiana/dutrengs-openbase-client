
/*
*
*	Database access ( Firebase is used as database atm. ).
* * * * * * * * * */
dt.db = {};

// Adds an appointment to the firebase.database().ref("appointments")
dt.db.addAppointment = function(data){
	var newApt = new dt.class.Appointment(data);
	var pushKey = firebase.database().ref().child('appointments').push().key;
	if(dt.sys.isDebugging){
		alert(newApt.title + ' ' + newApt.description);
		alert('Appointment was attempted to be created with key ' + pushKey);
		console.dir(newApt);
	}
	

	// Actually push the appointment to firebase.
	firebase.database().ref('appointments/'+pushKey).set(newApt,function(err){

		// If there is an error
		if(err){
			// Handle error
			alert('Neimen! Noe galt skjedde da du prøvde å lage en ny aktivitet. For noen klovner vi er noen ganger! Vennligst kontakt oss eller kanskje les \'Ofte stilte spørsmål\'-siden vår.');
		}else{

			// Broadcast to the world what a success this was!
			if(dt.sys.isDebugging)
				alert('Appointment was successfully created!')

			$('#CreateAppointmentModal').modal('hide');
			alert('Aktiviteten du lagde, er lastet opp! Tenk om du møter en ny bestevenn eller forretningspartner!');

		}
	});
}


//
// Get all appointments associated with a specific user.
// the only argument is a callback which returns a list over appointments
// Which the user has created.
dt.db.getAppointmentsCreatedByCurrentUser = function(cb){
	var user = firebase.auth().currentUser;
	firebase.database().ref('appointments').orderByChild('administrator').equalTo(user.uid).on('value',function(snapshot){
		
		if(dt.sys.isDebugging){
			alert(' Query: getAppointmentsCreatedByCurrentUser returns -> ' +snapshot.val());
			console.log(snapshot.val());				
		}

		// Return some data ( if any ) to the callback.
		cb(snapshot.val());	
		
	});
};

// Function used to get All the appointments in the system.
// Which people can currently attend to.
// The data is passed as the only argument to a programmer-provided callback.
dt.db.getAllActiveAppointments = function(cb){
	firebase.database().ref('appointments').on('value',function(snapshot){
		
		if(dt.sys.isDebugging){
			alert(' Query: getAllActiveAppointments returns -> ' +snapshot.val());
			console.log('getAllActiveAppointmentsReturnVal:');
			console.log(snapshot.val());				
		}

		// Return some data ( if any ) to the callback.
		cb(snapshot.val());	
		
	});
}


// Function that pushes updates to an appointment to firebase.
// And calls the userspeficied callback.
dt.db.updateAppointmentById = function(appointmentID,newData,cb){

	firebase.database().ref('appointments/'+appointmentID).update(newData).then(function(){
		// If update was successfull
		if(dt.sys.isDebugging){
			alert('Appointment was updated.');
		}

		cb(true)

	},function(err){
		// If update failed.
		if(dt.sys.isDebugging){
			alert('Appointment was not updated.')
		}

		cb(false);

	});

}

// Function that returns the amount of users that participate
// in an appointment and passes it to the callback-function cb
// For further processing.
dt.db.getParticipatingUsersInAppointment = function(appointmentID,cb){

	if(dt.sys.isDebugging){
		alert('Calling dt.db.getParticipatingUsersInAppointment with AppointmentID' + appointmentID);
	}

	firebase.database().ref('appointments/'+appointmentID+'/participatingUsers/').once('value',function(snapshot){
		if(dt.sys.isDebugging){
			alert('dt.db.getParticipatingUsersInAppointment returns -> ' + snapshot.val());
		}
		// return the data and pass it in to the callback.
		cb(snapshot.val());
	});

}

// Function that fetches all the appointments where a user participates.
// And passes it as an argument to the callback-cb.
dt.db.getAppointmentsWhereUserParticipates = function(cb){
	var user = firebase.auth().currentUser;
	firebase.database().ref('users/' + user.uid + '/appointmentsWhereUserTakesPart/').on('value',function(snapshot){
		

		if(dt.sys.isDebugging){
			alert(' Query: getAppointmentsWhereUserParticipates returns -> ' + snapshot.val());
			console.log('Appointments where user participates:')
			console.log(snapshot.val());				
		}

		// If there are no appointments
		// Just call the callback and notify
		// that there is no data there..
		if(!snapshot.val()){
			cb(snapshot.val());
		}

		// Snapshot.val() should hold ID's of appointments where the user takes part.

		var amountOfEvents = Object.keys(snapshot.val()).length;
		var fetchedAppointments = {};

		// Kick off fetching appointments from firebase.
		for(var aptID in snapshot.val()){
			dt.db.getAppointmentByID(aptID,function(data){
				fetchedAppointments[aptID] = data;
			});
		}


		// A function that checks if all appointment-data has been retrieved.
		var checkIfAllAppointmentsHaveArrived = function(){
			if(Object.keys(fetchedAppointments).length == amountOfEvents){
				cb(fetchedAppointments);
			}else{
				setTimeout(checkIfAllAppointmentsHaveArrived,100);
			}
		}();
		
	},function(err){
		// If fetching failed.
		if(dt.sys.isDebugging){
			alert('The fetching of Appointments the user takes part in failed.')
		}

	});
};

// Adds a user to an appointment in firebase.
dt.db.addUserToAppointmentById = function(userID,appointmentID,cb){
	firebase.database().ref('appointments/'+appointmentID+'/participatingUsers/'+userID).set(userID).then(function(){
		// If database write was successfull
		if(dt.sys.isDebugging){
			alert('User'+userID +'  joined the eventWithID ' + appointmentID);
		}

		// Add appointment to the users list
		firebase.database().ref('users/'+userID+'/appointmentsWhereUserTakesPart/'+appointmentID).set(true).then(function(snapshot){
			// If database write was successfull
			if(dt.sys.isDebugging){
				alert('users/UID/appointmentsWhereUserTakesPart/aptID was stored.');
			}
			cb(true);
		}).catch(function(err){
			// If database write failedl
			if(dt.sys.isDebugging){
				alert('users/UID/appointmentsWhereUserTakesPart/aptID never occured.');
			}
			cb(false);

		});
		
	}).catch(function(err){
		// If database write failed
		if(dt.sys.isDebugging){
			alert('appointments/aptID/participatingUsers/userID never occured.');
		}
		cb(false);
	});
};

// Removes a user from an Appointment in firebase.
dt.db.removeUserFromAppointment = function(userID,appointmentID,cb){
	firebase.database().ref('appointments/'+appointmentID+'/participatingUsers/'+userID).set(null).then(function(){
		// If database write was successfull
		// Give user feedback.
		if(dt.sys.isDebugging){
			alert('User Left an appointment.');
		}
		// Add appointment to the users list.
		firebase.database().ref('users/'+userID+'/appointmentsWhereUserTakesPart/'+appointmentID).set(null).then(function(snapshot){
			// If database write was successfull
			if(dt.sys.isDebugging){
				alert('users/UID/appointmentsWhereUserTakesPart/aptID was successfully deleted');
			}
			cb(true);
		}).catch(function(err){
			// If database write failed.
			if(dt.sys.isDebugging){
				alert('removing users/UID/appointmentsWhereUserTakesPart/aptID never occured');
			}
			cb(ƒalse);
		});;

	}).catch(function(err){
		// If database write failed.
		if(err){
			cb(false);
		}else{
			cb(true);
		}
		// If err
	});
}

// Fetches appointmentData for 1 appointment, ONCE, by ID
// and passes the data retrieved into the cb-callback function which
// is specified by the caller of the dt.db.function.
dt.db.getAppointmentByID = function(appointmentID,cb){
	firebase.database().ref('appointments/'+appointmentID).once('value',function(snapshot){
		if(dt.sys.isDebugging){
			alert(' Query: getAppointmentByID returns -> ' +snapshot.val());
			console.log(snapshot.val());				
		}

		// Return some data ( if any ) to the callback.
		cb(snapshot.val());	
	});
}

// Gets an appointment and calls the callback whenever the 
// appointment is updated
dt.db.getAppointmentByIDWatcher = function(appointmentID,cb){
	firebase.database().ref('appointments/'+appointmentID).on('value',function(snapshot){
		if(dt.sys.isDebugging){
			alert(' Query: getAppointmentByID returns -> ' +snapshot.val());
			console.log(snapshot.val());				
		}

		// Return some data ( if any ) to the callback.
		cb(snapshot.val());	
	});
}

// Returns how many empty spots there are in an appointment
// Which is stored in firebase.
dt.db.getEmptySpotCountForAppointmentByID = function(appointmentID,cb){

	if(dt.sys.isDebugging){
		alert('dt.db.getEmptySpotCoutnForAppointment was called!');
	}
	dt.db.getAppointmentByID(appointmentID,function(data){


		if(!data){
			cb(undefined);
			return;
		}

		var participantCount = Object.keys(data.participatingUsers).length;
		cb(data.maxUsers - participantCount);
	});
}

// Deletes an appointment and passes the resulting
// data to the user-specified callback-function 'cb'.
dt.db.deleteAppointmentById = function(appointmentID,cb){
	firebase.database().ref('appointments/'+appointmentID).set(null).then(function(snapshot){
		
		if(dt.sys.isDebugging){
			alert('User tried to delete an appointment the firebaseresponse was-> '+snapshot);
		}
		cb(snapshot.val());

	}).catch(function(){
		// Deleting did not go well.
		cb(null);

	});

}

// Returns a certain amount of random appointments within the currently visible area of a
// google.maps map , and passes it to the callback function cb, as an argument.
dt.db.getAppointmentsWithinMap = function(map,maxresults,cb){
	firebase.database().ref('appointments').once('value',function(snapshot){
		var allAppointments = snapshot.val();

		if(!allAppointments){
			cb(snapshot.val());
		}
		var bounds = map.getBounds();

		var appointmentsInsideMap = {};
		for(var a in allAppointments){
			var apt = allAppointments[a];
			// Checks is the current appointment being processed is inside
			// the specified map.
			if(apt.latlng.lat < bounds.getNorthEast().lat() && apt.latlng.lat > bounds.getSouthWest().lat()
			 && apt.latlng.lng < bounds.getNorthEast().lng() && apt.latlng.lng > bounds.getSouthWest().lng()) {
				appointmentsInsideMap[a] = allAppointments[a];
				if(Object.keys(appointmentsInsideMap).length >= maxresults)
					break;
			}
		}
		cb(appointmentsInsideMap)
	});

}


// Pushes a message to an appointment by ID
// and stores it in firebase.
// The user can provide a callback function cb which is supposed
// to hold an object that can be used to get feedback wether the
// pushing to firebase went OK.
dt.db.addMessageToAppointment = function(msg,appointmentID,cb){

	if(dt.sys.isDebugging){
		alert('dt.db.addMessageToAppointment was called.');
		console.log('arguments:');
		console.log('msg ' + msg);
		console.log('appointmentID' + appointmentID);
	}

	firebase.database().ref('appointments/'+appointmentID).child('messages').push(msg).then(function(snapshot){
		if(dt.sys.isDebugging){
			alert('firebase.database().ref(\'appointments/\'+appointmentID+\'/messages\').push(msg) returns ->'+snapshot.val());
		}
		// Pass the result of the write to the userdefined callback and call it.
		cb(snapshot.val())
	},function(err){

	});
}



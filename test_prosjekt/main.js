

openbase.auth.onAuthChanged = function(user){
	alert("onAuthChanged was called " + user);

	if(!user){
		$("#appointmentsDiv").html("");
		$("#UserNavArea .userNavDisplayNameArea").html("Not logged in.");
		$(".showOnLogin").hide();
		$(".hideOnLogin").show();
	}else{
		//$("#appointmentsDiv").load("/appointments");
		openbase.db.getDataByRoute("/appointments",function(data){
			$("#appointmentsDiv").html(JSON.stringify(data));
		});
		$("#UserNavArea .userNavDisplayNameArea").html(user.displayName);
		$(".showOnLogin").show();
		$(".hideOnLogin").hide();
	}
}

$(document).ready(function(){
	alert("document is ready bro.");
	openbase.init({
		domain:"http://128.199.33.216:3000",
		pollTime:2000
	});
});



/*
	UI used for testing purposes atm.. :) ;) ;) 
*/
signInButtonPressed = function(){
	var mail = $("#signInForm input[name='email']").val();
	var pw = $("#signInForm input[name='password']").val();
	openbase.auth.signIn(mail,pw);
}

createUserButtonPressed = function(){
	var mail = $("#createUserForm input[name='email']").val();
	var pw = $("#createUserForm input[name='password']").val();
	openbase.auth.createUser(mail,pw,function(data){
		alert("data");
	})
}

signOutButtonPressed = function(){
	openbase.auth.signOut();
}

updateUserButtonPressed = function(){
	var data = {
		"displayName" : $("#updateUserForm input[name='displayName']").val(),
		"photoURL"    : $("#updateUserForm input[name='photoURL']").val(),
	}
	openbase.auth.updateUser(data,function(data){
		alert("User attempted to be updated lol. data: " + data);
	});
}

createAppointmentButtonPressed = function(){
	var data = {
		"title" : $("#createAppointmentForm input[name='title']").val(),
		"description" : $("#createAppointmentForm input[name='description']").val(),
		"place" : $("#createAppointmentForm input[name='place']").val(),
		"time" : $("#createAppointmentForm input[name='time']").val(),
		"minUsers" : $("#createAppointmentForm input[name='minUsers']").val(),
		"maxUsers" : $("#createAppointmentForm input[name='maxUsers']").val(),
		"latlng" : $("#createAppointmentForm input[name='latlng']").val(),
		"categories" : $("#createAppointmentForm input[name='categories']").val(),
		"participatingUsers" : $("#createAppointmentForm input[name='participatingUsers']").val(),
		"messages" : $("#createAppointmentForm input[name='messages']").val(),
		"administrator" : openbase.auth.currentUser.userID
	}

	openbase.db.set("appointments",data,function(data){
		alert("Database writerequest callback returned from the server with: "+data.msg);
	});
}
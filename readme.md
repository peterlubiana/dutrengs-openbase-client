# Project Title

webapp-prototype-duTrengs

## Hensikt med prosjekt
Å få noe kode oppe og kjører NÅ, som vi kan teste på brukere umiddelbart, og som kan imponere investorer på operaen.
Hvis ikke er vi bare ord. ( Slik føler jeg det nå -Peter 05.09.2017 kl 17.17 ).

## Komme i gang

Prosjektet slik det ser ut nå bruker node.js og bower ( bare for å 'builde' prosjektet, ikke for noe backend greier.)


## Techs brukt.
Prosjektet bruker :
Bootstrap 4,
jQuery 3,
google firebase for backend,
Sass.

Alle enkle (og forenklende) teknologier som alle er kjente med for å komme i gang NÅ.

### Prerequisites

Du trenger node.js og bower for å builde og serve prosjektet lokalt.
Så du må installere det hvis du ikke har det.


1, Last ned Node.js herfra -> https://nodejs.org/en/download/


2, straks du har det, kjør node commanden: npm install -g bower

Nå kan du laste ned alle de kjedelige komponentene du trenger ( bah! tar bare 1 min, tjeh.)


3, cd inn i prosjektet og kjør 'npm install && bower install'
Du vet du har gjort rektig hvis du har node_modules og bower_component mapper i prosjektmappa, tjehe.
Nå kan du kjøre skiten med gulp. Det er nå det blir rått tjeh. !


4, gå inn i terminalen og skrive 'gulp serve' mens du er i rota av prosjektet for å lage en lokal server
   som dataen din automatisk går inn på i browseren din -> 127.0.0.1:9000, alle endringer du gjør i filene
   vil oppdateres direkte i browseren mens du holder på.


5, For å bygge en minifisert-versjon av prosjektet kan du skrive 'gulp build' og det vil dukke opp en '/dist'  mappe
   som du kan legge på en hvilken som helst server eller bare kjøre lokalt.


## tester

For å teste kan du sette dt.sys.isDebugging = true. i app/main.js fila. Du vil få driiitmange alerts opp.
Jeg har ikke skrevet noen dedikerte tester foreløpig, men det kan og bør komme etterhvert som prosjektet blir mer
seriøst.

## Deployment

Som skrevet over:
cmd$> 'gulp build'

Som lager en /dist mappe som du kan deploye.


## Bidra

Gjør hva dere vil, jeg stoler på dere.

## Authors / Kode-galninger

MrtAndPe

## License / Lisens

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
